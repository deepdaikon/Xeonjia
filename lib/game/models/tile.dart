import 'dart:math';

import 'package:flame/sprite.dart';
import 'package:xeonjia/game/xeonjia_game.dart';

/// Class used to manage a single tile
class Tile {
  Tile({
    this.id,
    this.gid,
    this.tiledClass,
    this.sprite,
    this.size,
    this.animationStepTime,
    this.position,
    this.layer,
    Map<String, dynamic>? properties,
  }) {
    size ??= componentSize;
    this.properties = properties ?? {};
  }

  /// Tile id and gid defined in the TMX file
  int? id;
  int? gid;

  /// Component class
  String? tiledClass;

  /// List of tile properties
  /// Properties define component features and stats (eg: atk, def, hp)
  late Map<String, dynamic> properties;

  /// Component sprite
  Sprite? sprite;
  bool get hidden => sprite == null;

  /// Component size
  double? size;

  /// Component animation
  List<Sprite> animationSprites = [];
  double? animationStepTime;

  /// Tile position
  Point? position;

  /// Map layer
  int? layer;
}
