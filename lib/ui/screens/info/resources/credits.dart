import 'package:xeonjia/utils/i18n.dart';

/// List of people who worked on this project
String get credits => '''
${'Thanks to everyone who contributed to this project!'.i18n}

# ${'Programmer'.i18n}: deepdaikon

# ${'Artists'.i18n}:
    * deepdaikon

# ${'Music by'.i18n}: Yubatake

# ${'Translators'.i18n}:
    * Alex
    * Alparslan Şakçi
    * Andrey
    * anselm-helbig
    * araccaine
    * Balázs Úr
    * Bruno Cabrita
    * cannnAvar
    * CaoWangrenbo
    * Carlos Henrique Maschio Martins
    * ctntt
    * deepdaikon
    * Denis Proleev
    * d4f5409d
    * Filip-K-87
    * Fontan 030
    * hamaryns
    * hooay233
    * Hrela
    * Hunter
    * H6rd
    * Iwan Gabovitch
    * Jorge Castro Nistal
    * J. Lavoie
    * Kempelen
    * KiberDoktor
    * KMX
    * libamidi
    * liimee
    * limbobytes
    * Lin.KH
    * Liu Tao
    * LS-Shandong
    * Lucas Melo Sanginetto
    * Mark Badrus
    * Martin
    * mimoguz
    * Muha Aliss
    * Nicolas
    * no no
    * notjatin
    * pascalou
    * Patryk
    * perro tuerto
    * Pink Serenity
    * PulsarTech
    * Quang Trung
    * randint
    * Satou
    * Sean R.
    * Sebastian
    * Sergiy Borodych
    * Sir Crownguard
    * snipe2004
    * Sniventals
    * Super12138
    * Swann Fournial
    * Tetro
    * Thothorg
    * Tomasz K.
    * tonykouki
    * Tri-fan
    * user3678169
    * Viktória Nagy
    * Vinicius
    * Vri
    * Vsnmrn
    * Xika Ka
    * xlf1024
    * YottaMxt
    * Y106
    * zwergwolf
    * 1479949473
    * 4545duckling
    * 914846037
''';
