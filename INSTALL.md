# Linux

Make sure the Linux system you are installing it on has all of the system libraries required.

On Ubuntu use:

```
sudo apt-get install libgtk-3-0 libblkid1 liblzma5 libgstreamer-plugins-base1.0-0
```
