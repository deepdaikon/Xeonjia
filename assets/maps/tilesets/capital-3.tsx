<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.0" name="capital-3" tilewidth="16" tileheight="16" tilecount="168" columns="4">
 <image source="../../images/capital-3.png" width="64" height="32"/>
 <tile id="0" class="Solid"/>
 <tile id="1" class="Solid"/>
 <tile id="2" class="Solid"/>
 <tile id="3" class="Solid"/>
 <tile id="4" class="Solid"/>
 <tile id="5" class="Solid"/>
 <tile id="6" class="Solid"/>
 <tile id="7" class="Solid"/>
</tileset>
