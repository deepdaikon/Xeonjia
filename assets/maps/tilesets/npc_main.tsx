<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.0" name="npc_main" tilewidth="16" tileheight="16" tilecount="32" columns="4">
 <image source="../../images/npc_main.png" width="64" height="128"/>
 <tile id="0" class="NPC">
  <properties>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="1" class="NPC">
  <properties>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="2" class="NPC">
  <properties>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="3" class="NPC">
  <properties>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="4" class="NPC">
  <properties>
   <property name="name" value="dr. lache"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="5" class="NPC">
  <properties>
   <property name="name" value="dr. lache"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="6" class="NPC">
  <properties>
   <property name="name" value="dr. lache"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="7" class="NPC">
  <properties>
   <property name="name" value="dr. lache"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="8" class="NPC">
  <properties>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="9" class="NPC">
  <properties>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="10" class="NPC">
  <properties>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="11" class="NPC">
  <properties>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="12" class="NPC">
  <properties>
   <property name="name" value="milla"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="13" class="NPC">
  <properties>
   <property name="name" value="milla"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="14" class="NPC">
  <properties>
   <property name="name" value="milla"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="15" class="NPC">
  <properties>
   <property name="name" value="milla"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="16" class="NPC">
  <properties>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="17" class="NPC">
  <properties>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="18" class="NPC">
  <properties>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="19" class="NPC">
  <properties>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="20" class="NPC">
  <properties>
   <property name="name" value="65th hero"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="21" class="NPC">
  <properties>
   <property name="name" value="65th hero"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="22" class="NPC">
  <properties>
   <property name="name" value="65th hero"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="23" class="NPC">
  <properties>
   <property name="name" value="65th hero"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="24" class="NPC">
  <properties>
   <property name="name" value="shen"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="25" class="NPC">
  <properties>
   <property name="name" value="shen"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="26" class="NPC">
  <properties>
   <property name="name" value="shen"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="27" class="NPC">
  <properties>
   <property name="name" value="shen"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="28" class="NPC">
  <properties>
   <property name="name" value="september"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="29" class="NPC">
  <properties>
   <property name="name" value="september"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="30" class="NPC">
  <properties>
   <property name="name" value="september"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="31" class="NPC">
  <properties>
   <property name="name" value="september"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
