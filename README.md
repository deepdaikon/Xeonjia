# X E O N J i A

Save the world by solving ice puzzles and defeating enemies!

Available on Android, Windows and Linux.

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="70">](https://play.google.com/store/apps/details?id=xyz.deepdaikon.xeonjia)
[<img src="https://static.itch.io/images/badge.svg"
     alt="Get it on itch.io"
     height="45">](https://deepdaikon.itch.io/xeonjia)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="70">](https://f-droid.org/packages/xyz.deepdaikon.xeonjia/)

## About

[![Liberapay](https://img.shields.io/liberapay/patrons/deepdaikon.svg?logo=liberapay)](https://liberapay.com/deepdaikon/donate)
[<img src="https://www.ko-fi.com/img/githubbutton_sm.svg" alt="Ko-fi" height="20">](https://ko-fi.com/deepdaikon)
[<img src="https://translate.deepdaikon.xyz/widgets/xeonjia/-/svg-badge.svg" alt="Translation status" height="20">](https://translate.deepdaikon.xyz/engage/xeonjia/)

**Xeonjia** is an **adventure game** set in a **frozen world**.

The floor is frozen! Think carefully about your moves because you can't change direction until you
meet an obstacle.
Use you mind to solve the ice slide puzzles!

The world has been frozen by the **"King of Evil"** and it's no longer a safe place.

Legend has it that a brave hero will defeat the King of Evil and save the kingdom… For this reason
every year a person is appointed as "hero" and sent on a journey to try to save it.

Now it's your time, **you have been chosen as this year's hero!** Will you be able to defeat the
King of Evil?

You will have to travel the world, explore cities, dungeons and mystical caves.

**Are you ready to solve quests and puzzles in this frozen RPG world?**

Over time, you will gain experience points that will allow you to increase your level and you will
find money and hidden treasures that you can use to buy stuff and improve yourself.

Be careful, the world is full of **dangerous enemies** ready to attack you!

Keep in mind that most of **the floor is frozen**, so you can't stop yourself until you reach a
wall, a boulder, or any other type of obstacle.

**Use your mind to figure out the best path!**

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/0_Home.png" height="320">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1_Story.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/2_Story.png" height="320">
<img src ="fastlane/metadata/android/en-US/images/phoneScreenshots/3_Story.png" height="320">

## Donate

If you want to support the development of Xeonjia you can donate through Liberapay or Ko-fi.

[<img src="https://liberapay.com/assets/widgets/donate.svg" alt="Donate" height="30">](https://liberapay.com/deepdaikon/donate)
[<img src="https://cdn.ko-fi.com/cdn/kofi3.png?v=2" alt="Donate" height="30">](https://ko-fi.com/deepdaikon)

## Contribute

Any contribution is highly appreciated.

Are you a pixel artist? A musician? A storywriter? A developer? None of these but you just want to
help?

Write me an email at: deepdaikon@tuta.io :)

## Translate

You can use [Weblate](https://translate.deepdaikon.xyz/engage/xeonjia/) if you want to help with the
translation of Xeonjia.
If your language is missing click on the
["Start new translation"](https://translate.deepdaikon.xyz/projects/xeonjia/story/) button.

[<img src="https://translate.deepdaikon.xyz/widgets/xeonjia/-/multi-auto.svg" alt="Translation status">](https://translate.deepdaikon.xyz/engage/xeonjia/)

Keep in mind that you need to be signed in Weblate to translate Xeonjia. Your username and email
address will be placed in the translated files to keep track of the co-authors of the translations.

You can make anonymous suggestions if you don't want to sign in.

Thank you :)

## What people say

Some of the reviews and comments received:

- "Xeonjia is a very unique and enjoyable game to play. With pixel art graphics it mixes RPGs with
puzzle games!" - [Le Alternative](https://www.lealternative.net/2020/12/23/xeonjia/)

- "I never knew a mobile game could be this fun, but I love everything about Xeonjia. The puzzles
were just the right difficulty, and I love the music and visual design as well as story. Thank
you!!!" - Ra

- "It's a free, open source RPG. I don't think people have really found this app yet, but they
will." - M.T.

- "I found it a really interesting and clever idea." - R.V.

- "Thanks for developing this game! I enjoy ice puzzles in games, so this is a real treat to play." -
K.

- "Nice game, cool mechanic. I like the puzzle aspect of the navigation. It can be tedious, but it's
part of the fun." - G.L.

- "Me encantó." - J.O.

## License

### Code

This software is distributed under the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or any later version. See the file LICENSE for the
conditions under which this software is made available. This software also contains code from other
sources.

### Graphics

Except where otherwise noted, all the graphics of this game are licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License
([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)).

### Music

Except where otherwise noted, all the background music of this game are licensed under the Creative
Commons Attribution-ShareAlike 4.0 International License
([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)).

The following .oga files are adapted artwork from Yubatake and are licensed under the Creative
Commons Attribution 3.0 Unported License ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/))
: boss, city-1, city-2, city-3, cityhall, enemies, forest, gameover, item, king-of-evil, mystic,
route-1, route-2, route-3, tower, tree, village-1, village-2, village-3, win.
