<b>Xeonjia</b> 是一款 <b>奇幻風格的冒險遊戲</b> 設定在一個 <b>被寒冰所壟罩的世界當中</b>.

地表被完全結凍！再三考慮你的移動方向，因為在遇到障礙之前你不能改變方向喔。
用你的智慧解決難題！

這個世界被 <b>“邪惡帝王”</b> 所凍結，大地荒蕪，寸草不生。

遠古的傳說中將會出現勇者擊敗邪惡帝王，拯救王國… 所以每年都會選出一位勇者，並使他踏上旅途，嘗試拯救世界。

啊現在你被選中了， <b>【恭喜您今年被選為勇者！】</b> 你有辦法擊敗邪惡帝王嗎？

你必須環遊世界，探索城市、地下城還有神秘的洞窟。

<b>準備好在這個角色扮演遊戲中完成委託，解決難題嗎？</b>

隨著時間的推移，勇者將獲得經驗值，以提高您的等級。發現金幣和神秘寶箱，用它們來購買物品和強化自己的力量。

你最好注意你的四周，這個世界充滿了<b>危險的魔物</b>隨時會攻擊你！

記住，大多數的<b>地表都是結凍的</b>，在遇到牆壁、巨石或任何其他類型的障礙物之前，您停不下來。

<b>用你的智慧找出最佳路徑！</b>
