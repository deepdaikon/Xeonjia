A <b>Xeonjia</b> egy <b>fagyos világban</b> játszódó <b>kalandjáték</b>.

A talaj megfagyott! Gondold át alaposan a lépéseidet, mert nem tudsz irányt változtatni, amíg akadályba nem ütközöl.
Használd az eszed a jeges, csúszós fejtörők megoldásához!

A <b>„gonosz király”</b> befagyasztotta a világot, és többé már nem biztonságos hely.

A legenda szerint egy bátor hős legyőzi majd a gonosz királyt, és megmenti a királyságot… Ezért minden évben kineveznek egy személyt „hősnek”, és útnak indítják, hogy megpróbálja megmenteni a királyságot.

Most eljött a te időd, <b>téged választottak az idei év hősének!</b> Képes leszel legyőzni a gonosz királyt?

Be kell majd utaznod a világot, felfedezned városokat, várbörtönöket és misztikus barlangokat.

<b>Készen állsz a küldetések és fejtörők megoldására ebben a fagyos RPG-világban?</b>

Idővel tapasztalati pontokat szerzel majd, amelyek lehetővé teszik, hogy növeld a szinted, valamint pénzt és rejtett kincseket találsz, amelyeket dolgok vásárlásához és önmagad fejlesztéséhez használhatsz.

Légy óvatos, a világ tele van <b>veszélyes ellenségekkel</b>, akik készen állnak arra, hogy megtámadjanak!

Ne feledd, hogy a <b>talaj nagy része fagyott</b>, így nem tudsz megállni, amíg nem érsz el egy falat, egy sziklát vagy bármilyen más típusú akadályt.

<b>Használd az eszed, hogy megtaláld a legjobb utat!</b>
